var app = getApp();
function getHotelList(cb) {
    console.log('jiudian');
    app.util.request({
        url: 'entry/wxapp/hotellists',
        data: {
            m: 'ewei_hotel',
        },
        cachetime: 30,
        success: function (res) {
            if (res.data.errno == 0) {
                typeof cb == "function" && cb(res);
            } else {
                failGo(res.data.message);
            }
        },
        fail: function () {
            failGo('请检查连接地址');
        }
    })
}
function getHotelInfo(hid, page, order, cb) {
    var search_array = wx.getStorageSync('ewei_hotel_search_array');
    app.util.request({
        url: 'entry/wxapp/hotelroomlists',
        data: {
            m: 'ewei_hotel',
            hid: hid,
            page: page ? page : '',
            order_title: order ? order : '',
            bdate: search_array.bdate ? search_array.bdate : '',
            day: search_array.day ? search_array.day : ''
        },
        cachetime: 30,
        success: function (res) {
            if (res.data.errno == 0) {
                typeof cb == "function" && cb(res.data.data);
            } else {
                failGo(res.data.message);
            }
        },
        fail: function () {
            failGo('请检查连接地址');
        }
    })
}
function failGo(content) {
    wx.showModal({
        content: content,
        showCancel: false,
        success: function (res) {
            if (res.confirm) {
                wx.redirectTo({
                    url: '/ewei_hotel/pages/index/index'
                })
            }
        }
    })
}
module.exports = {
    'getHotelList': getHotelList,
    'getHotelInfo': getHotelInfo,
    'failGo': failGo
};