var app = getApp();
var js = require('../../resource/js/app.js');
Page({
    data: {
        id: null,
        hid: null,
        bdate: null,
        day: 1,
        nums: 1,
        total: 0
    },
    onLoad: function (option) {
        var that = this;
        if (option.id && option.hid) {
            wx.getStorage({
                key: 'ewei_hotel_search_array',
                success: function (res) {
                    if (res.data) {
                        that.setData({
                            id: option.id,
                            hid: option.hid,
                            bdate: res.data.bdate,
                            day: res.data.day
                        })
                        app.util.request({
                            url: 'entry/wxapp/order',
                            data: {
                                m: 'ewei_hotel',
                                hid: option.hid,
                                id: option.id,
                                bdate: res.data.bdate,
                                day: res.data.day,
                            },
                            success: function (res) {
                                console.log(js);
                                if (res.data.errno != 0) {
                                    js.failGo(res.data.message)
                                } else {
                                    var res = res.data.data;
                                    that.setData({
                                        order: res
                                    })

                                }
                            }
                        })
                    } else {
                        js.failGo('请选择日期')
                    }
                },
                fail: function () {
                    js.failGo('请选择日期')
                }
            })
        } else {

        }

    },
    bindDayChange: function (e) {
        var max = this.data.order.max_room;
        if (e.detail.value >= 0 && e.detail.value <= 8) {
            this.setData({
                'nums': Number(e.detail.value)
            })
        } else {
            wx.showModal({
                content: "最大房间数" + max,
                showCancel: false,
                success: function () {
                    this.setData({
                        'nums': 8
                    })
                }
            })
        }
    },
    input_subtract: function () {
        var nums = this.data.nums;
        var price = this.data.order.price;
        var service = this.data.order.service;
        nums = (nums - 1 > 1) ? nums - 1 : 1;
        var total = price * nums;
        this.setData({
            'nums': nums,
            'order.totalprice': parseFloat(total.toFixed(2))
        })
    },
    input_add: function () {
        var max = this.data.order.max_room;
        var nums = this.data.nums;
        nums = (nums + 1 > max) ? max : nums + 1;
        var price = this.data.order.price;
        var service = this.data.order.service;
        var total = price * nums;
        this.setData({
            'nums': nums,
            'order.totalprice': parseFloat(total.toFixed(2))
        })
    },
    formSubmit: function (e) {
        var that = this;
        var myreg = /^1[3|4|5|8][0-9]\d{4,8}$/;
        var uname = e.detail.value.uname;
        var contact_name = e.detail.value.contact_name;
        var mobile = e.detail.value.mobile;
        var error_msg = '请正确填写';
        if (!uname)
            error_msg = error_msg + '入住人 ';
        if (!contact_name)
            error_msg = error_msg + '联系人 ';
        if (mobile.length == 11 && myreg.test(mobile)) {
            mobile = 1;
        } else {
            mobile = 0
            error_msg = error_msg + '手机号 ';
        }
        if (uname && contact_name && mobile) {
            that.setData({
                loading: 1
            })
            app.util.request({
                url: 'entry/wxapp/order&m=ewei_hotel',
                data: e.detail.value,
                method: 'POST',
                header: {
                    'content-type': 'application/x-www-form-urlencoded'
                },
                success: function (res) {
                    if (!res.data.errno) {
                        wx.showToast({
                            title: '预订成功',
                            icon: 'success',
                            duration: 10000
                        })
                        wx.redirectTo({
                            url: '/ewei_hotel/pages/orderinfo/orderinfo?order=order&order_id=' + res.data.data.order_id
                        })
                    } else {
                        wx.showModal({
                            content: res.data.message
                        })
                    }
                },
                complete: function () {
                    wx.hideToast();
                }
            })
        } else {
            that.setData({
                uname_warn: uname ? '' : 1,
                contact_name_warn: contact_name ? '' : 1,
                mobile_warn: mobile ? '' : 1,
                error_msg: error_msg
            })
        }

    },
    bindchange: function () {
        this.setData({
            uname_warn: 0,
            contact_name_warn: 0,
            mobile_warn: 0,
            error_msg: 0
        })
    },
    formReset: function () {
        console.log('form发生了reset事件')
    }
})