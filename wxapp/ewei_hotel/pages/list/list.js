var js = require('../../resource/js/app.js');
var sliderWidth = 96; // 需要设置slider的宽度，用于计算中间位置
var app = getApp();
Page({
	data: {
		slider: {
			interval: 5000,
			duration: 1000
		},
		tabs: ["房型预定", "客户评价", "酒店详情"],
		activeIndex: "0",
		sliderOffset: 0,
		sliderLeft: 0,
		page: 1,
		total: 1,
		hid: null,
		thumbs: null,
		address: null,
		phone: null,
		map: {
			latitude: '',
			longitude: '',
		},
		search_array: {
			bdate: app.util.formatDate(new Date),
			day: 1
		},
		payment: null,
		rom_type: 0,
		room_type_list: null,
		room_list: null,
		comments: null,
		details: {
			device: '酒店设施', //酒店设施
			content: '订房说明', //订房说明
			description: '酒店介绍', //酒店介绍
			traffic: '位置交通', //位置交通
			sales: '促销信息' //促销信息
		}
	},
	//页面加载
	onLoad: function (option) {
		var that = this;
		app.util.footer(that);
		var hid = option.hid;
		that.setData({
			hid: option.hid
		})
		js.getHotelInfo(hid, '', '', function (res) {
			wx.setNavigationBarTitle({
				title: res.reply.title
			})
			that.setData({
				title: res.reply.title,
				page: 1,
				total: res.page_array.tpage,
				//幻灯片
				thumbs: res.thumbs,
				//位置
				address: res.reply.address,
				'map.longitude': res.reply.lng,
				'map.latitude': res.reply.lat,
				phone: res.reply.phone,
				//房间类型列表
				room_type_list: res.room_type,
				payment: res.payment,
				//房间列表
				room_list: res.room_list,
				//评论列表
				comments: res.comments,
				//详情
				'details.description': res.reply.description,
				'details.content': res.reply.content,
				'details.traffic': res.reply.traffic,
				'details.device': res.reply.device,
				'details.sales': res.reply.traffic,
			})
		});
		wx.getSystemInfo({
			success: function (res) {
				that.setData({
					sliderLeft: (res.windowWidth / that.data.tabs.length - sliderWidth) / 2
				});
			}
		});
		wx.getStorage({
			key: 'ewei_hotel_search_array',
			success: function (res) {
				if (res.data) {
					that.setData({
						search_array: res.data
					})
				} else {
					wx.setStorage({
						key: 'ewei_hotel_search_array',
						data: that.data.search_array
					})
				}
			},
			fail: function () {
				wx.setStorage({
					key: 'ewei_hotel_search_array',
					data: that.data.search_array
				})
			}
		})
	},
	onPullDownRefresh: function () {
		var that = this;
		js.getHotelInfo(that.data.hid, '', '', function (data) {
			wx.setNavigationBarTitle({
				title: data.reply.title
			}),
				that.setData({
					title: data.reply.title,
					page: 1,
					total: data.page_array.tpage,
					rom_type: 0,
					//幻灯片
					thumbs: data.thumbs,
					//位置
					address: data.reply.address,
					'map.longitude': data.reply.lng,
					'map.latitude': data.reply.lat,
					phone: data.reply.phone,
					//房间类型列表
					room_type_list: data.room_type,
					//房间列表
					room_list: data.room_list,
					payment: data.payment,
					//评论列表
					comments: data.comments,
					//详情
					'details.description': data.reply.description,
					'details.content': data.reply.content,
					'details.traffic': data.reply.traffic,
					'details.device': data.reply.device,
					'details.sales': data.reply.traffic,
				}),
				wx.stopPullDownRefresh()
		})
		wx.getStorage({
			key: 'ewei_hotel_search_array',
			success: function (res) {
				if (res.data) {
					that.setData({
						search_array: res.data
					})
				}
			}
		})
	},
	//tab切换
	tabClick: function (e) {
		this.setData({
			sliderOffset: e.currentTarget.offsetLeft,
			activeIndex: e.currentTarget.id
		});
	},
	//选择类型后
	bindPickerChange: function (e) {
		var that = this;
		that.setData({
			rom_type: Number(e.detail.value)
		})
		var rom_type_name = (e.detail.value != 0) ? this.data.room_type_list[e.detail.value] : '';
		js.getHotelInfo(that.data.hid, '', rom_type_name, function (data) {
			that.setData({
				page: 1,
				total: data.page_array.tpage,
				//房间列表
				room_list: data.room_list,
			})
		});
	},
	onReachBottom: function () {
		var that = this;
		if (that.data.page < that.data.total && that.data.activeIndex == 0) {
			that.setData({
				loading: 1
			})
			var rom_type_name = (that.data.room_type != 0) ? that.data.room_type_list[that.data.room_type] : '';
			js.getHotelInfo(that.data.hid, that.data.page + 1, rom_type_name, function (data) {
				that.setData({
					//房间列表
					room_list: that.data.room_list.concat(data.room_list),
					page: that.data.page + 1,
					loading: 0
				})
			});
		}
	},
	tel: function () {
		var that = this;
		wx.makePhoneCall({
			phoneNumber: that.data.phone //仅为示例，并非真实的电话号码
		})
	},
	showThumbs: function (e) {
		var that = this;
		var index = e.target.dataset.index;
		var thumbs_num = that.data.room_list[index].thumbs_num;
		if (thumbs_num) {
			wx.previewImage({
				urls: that.data.room_list[index].thumbs
			})
		}
	},
	hotelMap: function (e) {
		if (this.data.map.latitude && this.data.map.longitude) {
			wx.openLocation({
				latitude: Number(this.data.map.latitude),
				longitude: Number(this.data.map.longitude),
				scale: 28,
				name: this.data.title ? this.data.title : '微擎',
				fail: function () {
					wx.showModal({
						conctent: '找不到坐标',
						showCancel: false
					})
				}
			})
		} else {
			wx.showModal({
				conctent: '找不到坐标',
				showCancel: false
			})
		}

	}
})