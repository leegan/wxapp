var js = require('../../resource/js/app.js');
var app = getApp();
Page({
	data: {
		text: "Page orderlist",
		page: 1,
		total: 1
	},
	onLoad: function (options) {
		// 页面初始化 options为页面跳转所带来的参数
		var that = this;
		app.util.footer(that);
		app.util.request({
			url: 'entry/wxapp/OrderLists',
			data: {
				m: 'ewei_hotel',
			},
			cachetime: 30,
			success: function (res) {
				if (res.data.errno == 0) {
					var res = res.data.data;
					that.setData({
						page: 1,
						total: res.page_array.tpage,
						orderList: res.list
					})
				} else {
					js.failGo(res.data.message)
				}
			},
			fail: function() {
				js.failGo('获取失败')
			}
		})
	},
	onReady: function () {
		// 页面渲染完成
	},
	onShow: function () {
		// 页面显示
	},
	onHide: function () {
		// 页面隐藏
	},
	onUnload: function () {
		// 页面关闭
	},
	onReachBottom: function () {
		var that = this;
		if (that.data.page < that.data.total) {
			that.setData({
				loading: 1,
			})
			app.util.request({
			url: 'entry/wxapp/OrderLists',
			data: {
				m: 'ewei_hotel',
				page: that.data.page + 1
			},
			cachetime: 30,
			success: function (res) {
				if (res.data.errno == 0) {
					var res = res.data.data;
					that.setData({
						total: res.page_array.tpage,
						page: that.data.page + 1,
						orderList: that.data.orderList.concat(res.list),
						loading: 0
					})
				} else {
					js.failGo(res.data.message)
				}
			},
			fail: function() {
				js.failGo('获取失败')
			}
		})
		}
	},
})